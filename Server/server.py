import socket
import sys
import threading
import atexit
import time
import datamanager
import requestmanager


class ClientServer(threading.Thread):

    SendSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def __init__(self, socket):
        threading.Thread.__init__(self)
        self.client_sock = socket
        self.datamgr = datamanager.DataManager("localhost", "liwaa", "p@ss0rd", "testDatabase")
        self.flag = True

    # Code to run on Exit
    def exitprog(self):
        self.flag = False
        print "Exiting Server.. Cleaning up"
        self.client_sock.close()
        sys.exit(1)

    def run(self):
        flag = True
        self.client_sock.settimeout(1.0)
        username = ""
        while flag:
            try:
                message = self.client_sock.recv(1024)
                if message == "" or not message:
                    print "Etil hayda"
                    flag = False
                    break
                print message
                self.reqmgr = requestmanager.RequestManager(message, client_addr[0], client_addr[1], self)
                message = ""
                response = self.reqmgr.parsedata()
                if response.__contains__("auth:/"):
                    start = False
                    for c in response:
                        if c == '/':
                            start = True
                            continue
                        if start:
                            if c == '|':
                                break
                            username += c
                    print "User %s is in " % username
                    self.client_sock.sendall(response)
                    # self.client_sock.sendall(self.reqmgr.getonlinefriends(username))
                # print response
                else:
                    self.client_sock.sendall(response)
            except KeyboardInterrupt:
                self.reqmgr.setoffline(username)
                atexit.register(self.exitprog)
                self.client_sock.close()
                sys.exit(1)
            except socket.timeout:
                # print "TIME %s" %username
                if username != "":
                    self.client_sock.sendall(self.reqmgr.getonlinefriends(username))
                time.sleep(4.0)
                continue
            except socket.error, e:
                print "Error %s %s" % (username, e.message)
                if username != "":
                    self.reqmgr.setoffline(username)
                break
        self.reqmgr.setoffline(username)
        self.client_sock.close()

datamgr = datamanager.DataManager("localhost", "liwaa", "p@ss0rd", "testDatabase")
datamgr.setalloff()
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
address = ('localhost', 5565)
sock.bind(address)


sock.listen(20)
print "*************KebbeBlaban*************"
print "Server is Up and Running and Waiting "
print "*************************************"
while True:
    client_sock, client_addr = sock.accept()
    t = ClientServer(client_sock)
    t.start()
